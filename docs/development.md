# Development

This project uses poetry for dependency management and packaging/deployment to pypi.


### Update Packages

- `poetry update --dry-run`
- 'clean' local dev env: `poetry env remove $(which python)`
    - this is useful after you have removed a package and you want to 'clean' things


### Get A Development Instance

```
# get a shell
poetry shell
# install dependencies
poetry install

# run development instance of app
spw --version
```

### Publish To PyPi

- build: `poetry build`
- publish to pypi: `poetry publish --build`
